:: Terrain post 1
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry desert_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\desert*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry forest_fall_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\forest_fall*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry forest_lava_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\forest_lava*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry forest_spring_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\forest_spring*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry forest_summer_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\forest_summer*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry forest_winter_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\forest_winter*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry hills_ash_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\hills_ash*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry hills_fall_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\hills_fall*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry hills_spring_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\hills_spring*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry hills_summer_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\hills_summer*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry hills_winter_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\hills_winter*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry mountains_ash_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\mountains_ash*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry mountains_canyons_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\mountains_canyon*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry mountains_volcanic_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\mountains_volcanic*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry plains_spring_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\plains_spring*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry plains_summer_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\plains_summer*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry scrubland_fall_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\scrubland_fall*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry scrubland_lava_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\scrubland_lava*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry scrubland_spring_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\scrubland_spring*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry scrubland_summer_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\scrubland_summer*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry scrubland_water_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\scrubland_water*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry scrubland_winter_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\scrubland_winter*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry seacoast_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\seacoast*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry terrace_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\terrace*\*
rem "C:\Program Files\7-Zip\7z.exe" a -tzip -ry iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\*

:: Terrain post 2
::"C:\Program Files\7-Zip\7z.exe" a -tzip -ry arctic_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\arctic*\*
::"C:\Program Files\7-Zip\7z.exe" a -tzip -ry desert_base_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\desert_base*\*
::"C:\Program Files\7-Zip\7z.exe" a -tzip -ry forest_base_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\forest_base*\*
::"C:\Program Files\7-Zip\7z.exe" a -tzip -ry grass_spring_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\grass_spring*\*
::"C:\Program Files\7-Zip\7z.exe" a -tzip -ry grass_summer_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\grass_summer*\*
::"C:\Program Files\7-Zip\7z.exe" a -tzip -ry grass_winter_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\grass_winter*\*
::"C:\Program Files\7-Zip\7z.exe" a -tzip -ry hotsprings_base_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\hotsprings_base*\*
::"C:\Program Files\7-Zip\7z.exe" a -tzip -ry lava_base_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\lava_base*\*
::"C:\Program Files\7-Zip\7z.exe" a -tzip -ry mountains_clay_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\mountains_clay*\*
::"C:\Program Files\7-Zip\7z.exe" a -tzip -ry rollinghills_arctic_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\rollinghills_arctic*\*
::"C:\Program Files\7-Zip\7z.exe" a -tzip -ry rollinghills_desert_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\rollinghills_desert*\*
::"C:\Program Files\7-Zip\7z.exe" a -tzip -ry rollinghills_green_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\rollinghills_green*\*
::"C:\Program Files\7-Zip\7z.exe" a -tzip -ry rollinghills_lava_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\rollinghills_lava*\*
::"C:\Program Files\7-Zip\7z.exe" a -tzip -ry rollinghills_wetlands_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\rollinghills_wetlands*\*
::"C:\Program Files\7-Zip\7z.exe" a -tzip -ry sanddunes_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\sanddunes*\*
::"C:\Program Files\7-Zip\7z.exe" a -tzip -ry scrubland_base_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\scrubland_base*\*
::"C:\Program Files\7-Zip\7z.exe" a -tzip -ry seacoast_base_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\seacoast_base*\*
::"C:\Program Files\7-Zip\7z.exe" a -tzip -ry spring_base_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\spring_base*\*
::"C:\Program Files\7-Zip\7z.exe" a -tzip -ry swampland_base_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\swampland_base*\*
::"C:\Program Files\7-Zip\7z.exe" a -tzip -ry wasteland_base_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\wasteland_base*\*
::"C:\Program Files\7-Zip\7z.exe" a -tzip -ry grass_fall_iso_terrain_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Isometric*\Terrain*\grass_fall*\*

"C:\Program Files\7-Zip\7z.exe" a -tzip -ry iso_terrain2_patches_dgw.zip C:\_DATA\Dundjinni\PS_Objects\Iso\Terrain2\patches\Isometric*\Terrain*\*
